package main

import (
	"sort"
	"strconv"
	"strings"
)

type Set map[int]int

func (s Set) String() string {
	var out []string

	for key, _ := range s {
		out = append(out, strconv.Itoa(key))
	}

	return strings.Join(out, ", ")
}

func (s Set) Add(n int) {
	s[n]++
}

func (s Set) Union(set Set) {
	for key, _ := range set {
		s.Add(key)
	}
}

func (s Set) Values() []int {
	var v []int

	for key, _ := range s {
		v = append(v, key)
	}

	sort.Ints(v)
	return v
}

type FilterFunc func(key int, count int) bool

func (s Set) Filter(fn FilterFunc) Set {
	set := make(Set)

	for key, count := range s {
		if fn(key, count) {
			set.Add(key)
		}
	}
	return set
}
