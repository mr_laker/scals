package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/alecthomas/kong"
	"github.com/alecthomas/participle/v2"
)

var cli struct {
	Debug      bool     `help:"Print AST for expression."`
	Expression []string `arg required help:"Expression to evaluate."`
}

var parser = participle.MustBuild(
	&Expression{},
	participle.Lexer(expressionLexer),
	participle.Elide("Whitespace"),
	participle.UseLookahead(2),
)

func main() {
	ctx := kong.Parse(&cli,
		kong.Description("Sets calculator"),
		kong.UsageOnError(),
	)

	expr := &Expression{}
	err := parser.ParseString("", strings.Join(cli.Expression, " "), expr)
	ctx.FatalIfErrorf(err)

	if cli.Debug {
		json, err := json.MarshalIndent(expr, "", "    ")
		ctx.FatalIfErrorf(err)

		fmt.Printf("%v\n", string(json))
	}

	set, err := expr.Eval()
	ctx.FatalIfErrorf(err)

	for _, v := range set.Values() {
		fmt.Println(v)
	}
}
