package main

import (
	"bufio"
	"os"
	"strconv"
)

func readFile(fileName string) (Set, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	set := make(Set)
	s := bufio.NewScanner(f)

	for s.Scan() {
		i, err := strconv.Atoi(s.Text())
		if err != nil {
			return nil, err
		}

		set.Add(i)
	}

	return set, nil
}
