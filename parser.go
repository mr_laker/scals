package main

import (
	"errors"

	"github.com/alecthomas/participle/v2/lexer"
	"github.com/alecthomas/participle/v2/lexer/stateful"
)

var expressionLexer = lexer.Must(stateful.NewSimple([]stateful.Rule{
	{"Ident", `[a-zA-Z_][a-zA-Z0-9_\.\/]*`, nil},
	{"Int", `(\d*)?\d+`, nil},
	{"Punct", `[-[!@#$%^&*()+_={}\|:;"'<,>.?/]|]`, nil},
	{"Whitespace", `[ \t\n\r]+`, nil},
}))

var ErrOperatorNotFound = errors.New("unsupported operator")

type Operator string

var operators = map[Operator]func(f int) FilterFunc{
	"EQ": func(f int) FilterFunc { return func(key, count int) bool { return count == f } },
	"LE": func(f int) FilterFunc { return func(key, count int) bool { return count < f } },
	"GR": func(f int) FilterFunc { return func(key, count int) bool { return count > f } },
}

func (o *Operator) Capture(s []string) error {
	*o = Operator(s[0])
	return nil
}

func (o Operator) Eval(filter int, s Set) (Set, error) {
	fn, ok := operators[o]
	if !ok {
		return nil, ErrOperatorNotFound
	}

	return s.Filter(fn(filter)), nil
}

type File struct {
	Name string `@Ident`
}

func (f *File) Eval() (Set, error) {
	set, err := readFile(f.Name)
	if err != nil {
		return nil, err
	}

	return set, nil
}

type Term struct {
	Operator       Operator      `@("EQ" | "LE" | "GR")`
	Filter         int           `@Int`
	Files          []*File       `@@*`
	Subexpressions []*Expression `@@*`
}

func (t *Term) Eval() (Set, error) {
	set := make(Set)

	for _, f := range t.Files {
		s, err := f.Eval()
		if err != nil {
			return nil, err
		}
		set.Union(s)
	}

	for _, sub := range t.Subexpressions {
		s, err := sub.Eval()
		if err != nil {
			return nil, err
		}

		set.Union(s)
	}

	return t.Operator.Eval(t.Filter, set)
}

type Expression struct {
	Op *Term `"[" @@ "]"`
}

func (e *Expression) Eval() (Set, error) {
	return e.Op.Eval()
}
