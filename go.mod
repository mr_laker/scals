module example.com/scals

go 1.15

require (
	github.com/alecthomas/kong v0.2.12
	github.com/alecthomas/participle/v2 v2.0.0-alpha3
)
