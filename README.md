# Grammar of calculator is given:

* expression := “[“ operator N sets “]”
* sets := set | set sets
* set := file | expression
* operator := “EQ” | “LE” | “GR”

* “file” is a file with sorted integers, one integer in a line.
* “N” is a positive integer
# Meaning of operators:
* EQ - returns a set of integers which consists only from values which exists in exactly N sets - arguments of operator
* LE - returns a set of integers which consists only from values which exists in less then N sets - arguments of operator
* GR - returns a set of integers which consists only from values which exists in more then N sets - arguments of operator

# Example:

    $ ./scals [ GR 1 testdata/c.txt [ EQ 3 testdata/a.txt testdata/a.txt testdata/b.txt ] ]
    2
    3

    $ ./scalc [ LE 2 testdata/a.txt [ GR 1 testdata/b.txt testdata/c.txt ] ]
    1
    4
