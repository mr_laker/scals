FROM golang:1.15-buster AS builder

ENV APP_PATH  /opt/app

RUN set -ex \
    && buildDeps=' \
        gcc \
        musl-dev \
        git \
    ' \
    && apt-get update && apt-get install -y $buildDeps --no-install-recommends

COPY . $APP_PATH
WORKDIR ${APP_PATH}

RUN scripts/build.sh
