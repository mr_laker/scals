package main

import (
	"reflect"
	"testing"
)

var evalTestCases = []struct {
	Raw    string
	Values []int
}{
	{Raw: "[ EQ 1 testdata/a.txt ]", Values: []int{1, 2, 3}},
	{Raw: "[ LE 2 testdata/c.txt testdata/b.txt ]", Values: []int{1, 5}},
	{Raw: "[ GR 1 testdata/c.txt [ EQ 3 testdata/a.txt testdata/a.txt testdata/b.txt ] ]", Values: []int{2, 3}},
	{Raw: "[ LE 2 testdata/a.txt [ GR 1 testdata/b.txt testdata/c.txt ] ]", Values: []int{1, 4}},
	{Raw: "[ LE 2 testdata/c.txt [ EQ 1 testdata/a.txt ] [ EQ 2 testdata/b.txt testdata/c.txt ]]", Values: []int{5}},
}

func TestExpressionEval(t *testing.T) {
	for idx, test := range evalTestCases {
		expr := &Expression{}

		if err := parser.ParseString("", test.Raw, expr); err != nil {
			t.Errorf("Test %d parse error: %v", idx, err)
		}

		set, err := expr.Eval()
		if err != nil {
			t.Errorf("Test %d eval error: %v", idx, err)
		}

		if !reflect.DeepEqual(set.Values(), test.Values) {
			t.Errorf("Test %d expected:'%v'\nActual: %v", idx, test.Values, set.Values())
		}
	}
}

func BenchmarkExpressionSimple(b *testing.B) {
	expr := &Expression{}
	raw := "[ LE 2 testdata/a.txt ]"

	for i := 0; i < b.N; i++ {
		if err := parser.ParseString("", raw, expr); err != nil {
			b.Fatal(err)
		}

		if _, err := expr.Eval(); err != nil {
			b.Fatal(err)
		}
	}
}

func BenchmarkExpressionWithSub(b *testing.B) {
	expr := &Expression{}
	raw := "[ LE 2 testdata/a.txt [ GR 1 testdata/b.txt testdata/c.txt ] ]"

	for i := 0; i < b.N; i++ {
		if err := parser.ParseString("", raw, expr); err != nil {
			b.Fatal(err)
		}

		if _, err := expr.Eval(); err != nil {
			b.Fatal(err)
		}
	}
}
