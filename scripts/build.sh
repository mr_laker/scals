#!/bin/bash
srcPath="."
pkgFile="."
outputPath="build"

build () {
    output="$outputPath/$1"
    src="$srcPath/$pkgFile"

    printf "\nBuilding: $1\n"

    time go build -o $output $src

    printf "\nBuilt: $1 size:"
    ls -lah $output | awk '{print $5}'
    printf "\nDone building: $1\n\n"
}

build "scals"
exit 0
