package main

import (
	"testing"
)

var setTestCases = []struct {
	Key   int
	Count int
}{
	{Key: 1, Count: 3},
	{Key: 13, Count: 5},
	{Key: 7, Count: 4},
	{Key: 22, Count: 11},
	{Key: 17, Count: 7},
}

func TestSetAdd(t *testing.T) {
	set := make(Set)

	for idx, test := range setTestCases {
		for i := 0; i < test.Count; i++ {
			set.Add(test.Key)
		}

		if set[test.Key] != test.Count {
			t.Errorf("Test %d expected:'%d'\nActual: %d", idx, test.Count, set[test.Key])
		}
	}
}

func TestSetFilter(t *testing.T) {
	set := make(Set)

	for _, test := range setTestCases {
		for i := 0; i < test.Count; i++ {
			set.Add(test.Key)
		}
	}

	t.Run("filter set by key", func(t *testing.T) {
		s := set.Filter(func(key, count int) bool {
			return key == 2
		})

		if len(s) != 0 {
			t.Errorf("Test expected empty set\nActual: %v", s)
		}

		s = set.Filter(func(key, count int) bool {
			return key < 10
		})

		if len(s) != 2 {
			t.Errorf("Test expected set length 2\nActual: %v", s)
		}
	})

	t.Run("filter set by count", func(t *testing.T) {
		s := set.Filter(func(key, count int) bool {
			return count == 2
		})

		if len(s) != 0 {
			t.Errorf("Test expected empty set\nActual: %v", s)
		}

		s = set.Filter(func(key, count int) bool {
			return count == 7
		})

		if len(s) != 1 {
			t.Errorf("Test expected set length 2\nActual: %v", s)
		}
	})
}
